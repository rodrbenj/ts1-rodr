package cz.cvut.fel.ts1;

public class rodrbenj{
    public static int factorial (int n){
        int result=1,i=1;
        while(i<=n){
            result=result*i;
            i++;
        }

        return result;
    }
    public static int factorialrecurs(int n){
        if(n!=2) {
            int x=factorialrecurs(n-1);
            int y=n*x;
            return y;
        }
        else {
            return 2;
        }
    }

}