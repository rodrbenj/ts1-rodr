package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class rodrbenjTest {
    @Test
    public void factorialTest() {
        rodrbenj main = new rodrbenj();
        int n=2;
        long expectedResult = 2;
        long result = rodrbenj.factorial(n);
        Assertions.assertEquals(expectedResult,result);
    }
    @Test
    public void factorialTestrecurs() {
        rodrbenj main = new rodrbenj();
        int n=2;
        long expectedResult = 2;
        long result = rodrbenj.factorialrecurs(n);
        Assertions.assertEquals(expectedResult,result);
    }
}